#include <stdlib.h>
#include "stdio.h"
#include <string.h>

#include "stm8s.h"


void delay(uint32_t value)
{
  for (uint32_t i=0; i < value; i)
    ;
}

void carka()
{
    GPIO_Init(GPIOG, GPIO_PIN_4, GPIO_MODE_OUT_PP_LOW_SLOW);
    GPIO_WriteHigh(GPIOG, GPIO_PIN_4);
    delay(1000000);
    GPIO_WriteLow(GPIOG, GPIO_PIN_4);
    delay(750000);
}

void tecka()
{
    GPIO_Init(GPIOG, GPIO_PIN_4, GPIO_MODE_OUT_PP_LOW_SLOW);
    GPIO_WriteHigh(GPIOG, GPIO_PIN_4);
    delay(350000);
    GPIO_WriteLow(GPIOG, GPIO_PIN_4);
    delay(750000);
}

void mezera()
{
    GPIO_Init(GPIOG, GPIO_PIN_4, GPIO_MODE_OUT_PP_LOW_SLOW);
    GPIO_WriteLow(GPIOG, GPIO_PIN_4);
    delay(500000);
}

int main(void)
{
  CLK_HSIPrescalerConfig(CLK_PRESCALER_HSIDIV1); // FREQ MCU 16MHz
  GPIO_Init(GPIOC, GPIO_PIN_5, GPIO_MODE_OUT_PP_LOW_SLOW);
  

  // Slovo "Ahoj" v morseově abecedě
  /* tecka();
  carka();

  mezera();

  tecka();
  tecka();
  tecka();
  tecka();

  mezera();

  carka();
  carka();
  carka();

  mezera();

  tecka();
  carka();
  carka();
  carka();

  mezera();
  mezera();
  mezera();
  mezera();
  mezera();
 */


  char veta[100];
  char vystup[100];
  int l = 0;
  
  carka();
  carka();
  carka();
  
  printf("Zadejte větu: ");
  fgets (veta, 100, stdin);


    for (int i = 0; strlen(veta); i++)
    {
      switch(veta[i]) 
      {
        case 65:      //65=A
        tecka();
        carka();
        vystup[i]=65;
        mezera();
        break;
        case 66:    //B
        carka();
        tecka();
        tecka();
        tecka();
        vystup[i]=66;
        mezera();
        break;
        case 67:    //C
        carka();
        tecka();
        carka();
        tecka();
        vystup[i]=67;
        mezera();
        break;
        case 68:    //D
        carka();
        tecka();
        tecka();
        vystup[i]=68;
        mezera();
        break;
        case 69:    //E
        tecka();
        vystup[i]=69;
        mezera();
        break;
        case 70:    //F
        tecka();
        tecka();
        carka();
        tecka();
        vystup[i]=70;
        mezera();
        break;
        case 71:    //G
        carka();
        carka();
        tecka();
        vystup[i]=71;
        mezera();
        break;
        case 72:    //H
        tecka();
        tecka();
        tecka();
        tecka();
        vystup[i]=72;
        mezera();
        break;
        case 73:    //I
        tecka();
        tecka();
        vystup[i]=73;
        mezera();
        break;
        case 74:    //J
        tecka();
        carka();
        carka();
        carka();
        vystup[i]=74;
        mezera();
        break;
        case 75:    //K
        carka();
        tecka();
        carka();
        vystup[i]=75;
        mezera();
        break;
        case 76:    //L
        tecka();
        carka();
        tecka();
        tecka();
        vystup[i]=76;
        mezera();
        break;
        case 77:    //M
        carka();
        carka();
        vystup[i]=77;
        mezera();
        break;
        case 78:    //N
        carka();
        tecka();
        vystup[i]=78;
        mezera();
        break;
        case 79:    //O
        carka();
        carka();
        carka();
        vystup[i]=79;
        mezera();
        break;
        case 80:    //P
        tecka();
        carka();
        carka();
        tecka();
        vystup[i]=80;
        mezera();
        break;
        case 81:    //Q
        carka();
        carka();
        tecka();
        carka();
        vystup[i]=81;
        mezera();
        break;
        case 82:    //R
        tecka();
        carka();
        tecka();
        vystup[i]=82;
        mezera();
        break;
        case 83:    //S
        tecka();
        tecka();
        tecka();
        vystup[i]=83;
        mezera();
        break;
        case 84:    //T
        carka();
        vystup[i]=84;
        mezera();
        break;
        case 85:    //U
        tecka();
        tecka();
        carka();
        vystup[i]=85;
        mezera();
        break;
        case 86:    //V
        tecka();
        tecka();
        tecka();
        carka();
        vystup[i]=86;
        mezera();
        break;
        case 87:    //W
        tecka();
        carka();
        carka();
        vystup[i]=87;
        mezera();
        break;
        case 88:    //X
        carka();
        tecka();
        tecka();
        carka();
        vystup[i]=88;
        mezera();
        break;
        case 89:    //Y
        carka();
        tecka();
        carka();
        carka();
        vystup[i]=89;
        mezera();
        break;
        case 90:    //Z
        carka();
        carka();
        tecka();
        tecka();
        vystup[i]=90;
        mezera();
        break;
        case 32:  // (mezera)
        mezera();
        mezera();
        mezera();
        vystup[i]=32;
        delay(500);
        break;  
        }  
      }
      return 0;
}
