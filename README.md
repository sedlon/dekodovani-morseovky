# Dekódování morseovky



## Úvod

Do konzole napíšete text, který chcete dekódovat do morseovy abecedy. Výstupem je piezoelektrický bzučák, který vypípe již zdekódovaný text v morseově abecedě. *(Do konzole pište pouze VELKÁ písmena bez diakritiky)*.

## Seznam součástek

| **Typ součástky** | **Hodnota** | **Počet kusů** | **Cena za kus** | **Cena celkem** |
|:---:              |:---:        |:---:           |:---:            |:---:            |
|Piezoelektrický bzučák|          |       1        |      38 Kč      |      38 Kč      |
|Rezistor| 640 a 7300 Ω           |       5        |        3 Kč     |        15 Kč    |
|Kondenzátor        |      100nF  |       2        |       2,50 Kč   |          5 Kč   |
|Tranzistor         |             |       3        |          2 Kč   |       6 Kč      |
|*Cena celkem*      |             |                |                 |       64 Kč     |


## Blokové schéma

```mermaid
graph TD;
    PC-->STM8;
    STM8-->HARDWAROVÉ-ŘEŠENÍ-BZUČÁKU;
    HARDWAROVÉ-ŘEŠENÍ-BZUČÁKU-->BZUČÁK;
```

## Schéma

![alt text](schema-mit.jpg)

## Fotka zařízení

![alt text](fotkaprojekt.jpg)

## Kód

Je součástí přílohy a je pojmenován main.c

## Závěr

Kdybych si nyní mohl vybrat jiné téma, tak to udělám, neboť jsem až po zahájení zjistil že programovací jazyk C nemá stringy. Nejjednodušší bylo si vytvořit funkce, které znázorňují tečku a čárku v morseově abecedě. A asi nejtěžší bylo vymyslet jak získat od uživatele větu a následně ji převést do morseovy abecedy. Také jsem si musel ručně přidat knihovnu STDIO, protože mi jinak nefungovala. 

### Autor

Martin Sedlák
